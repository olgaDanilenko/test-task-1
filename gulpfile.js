var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    watch = require('gulp-watch'),
    connect = require('gulp-connect');

// server connect
gulp.task('connect', function() {
    connect.server({
        root: 'app',
        livereload: true
    });
});

// css
gulp.task('sass', function () {
    gulp.src('app/styles/*.sass')
        .pipe(plumber())
        .pipe(sass({errLogToConsole: true}))
        .pipe(connect.reload())
});

// html
gulp.task('html', function () {
    gulp.src('app/*.html')
        .pipe(connect.reload());
});

//watch
gulp.task('watch', function ()
{
    gulp.watch('app/styles/*.sass', ['sass']);
    gulp.watch('app/*.html', ['html']);
});

//default
gulp.task('default', ['connect', 'sass', 'html', 'watch']);
